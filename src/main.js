import App from './App.svelte';

let title = null;
const hash = window.location.hash;
const parsed = hash.match(/^#(?:([^#]*)#)?(.*)$/);
let gloozes = [];
if (parsed != null) {
    title = decodeURIComponent(parsed[1] ?? null);
    let contents = atob(decodeURIComponent(parsed[2] ?? ''));
    if (title?.length > 0) {
        document.title = title + " - Glooze";
    }
	gloozes = contents.split('\0').map(pair => {
		const splitOnEquals = pair.split('=');
		if (splitOnEquals.length !== 2) {
			return null;
		}
		return {
			prompt: splitOnEquals[0],
			answer: splitOnEquals[1],
		}
	}).filter(g => g !== null);
}

if (gloozes.length === 0 && title == null) {
	title = 'finskt i sjön';
	gloozes = [
		{
			answer: 'olla',
			prompt: 'att vara',
		},
		{
			answer: 'puhua',
			prompt: 'att prata',
		},
		{
			answer: 'sanoa',
			prompt: 'att säga',
		},
		{
			answer: 'kirjoittaa',
			prompt: 'att skriva',
		},
	];
}

const app = new App({
	target: document.body,
	props: {title, gloozes}
});

export default app;
